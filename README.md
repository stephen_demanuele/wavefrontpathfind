Find a path through obstacles, using WaveFront algorithm.

Create your environment by 
1. setting the world size, in terms of rows and columns
2. setting start and destination
3. setting obstacles

You can then find the shortest path from start to destination, avoiding the obstacles.

You can save the environment you created and load it again. 

Naive implementation, uses a datatable to simulate the cells of the real-world.

TODO:
* convert UI to WPF
* convert to HTML5, javascript

Inspired by  article at http://pirate.shu.edu/~wachsmut/Teaching/CSAS-Robotics/Challenges/06_Wavefront/index.html, has some modifications.
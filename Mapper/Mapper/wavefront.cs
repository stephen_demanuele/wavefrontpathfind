using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
 *  This is an implementation of a modified form of the Wavefront Algorithm 
 *  which can be found on 
 *  http://pirate.shu.edu/~wachsmut/Teaching/CSAS-Robotics/Challenges/06_Wavefront/index.html
 *  http://www.instructables.com/id/Adaptive-Mapping-and-Navigation-with-iRobot-Create/step6/Adaptive-Mapping-and-Wavefront-Algorithm/
 *
 *  NOTE:- Adjacent cells only represent the cells which can be reached from the the source by one hop
 *  in any primary direction (only E-W-N-S)
 */
public class Wavefront
{
    /// <summary>
    /// Enum representing the various states of the grid cells.
    /// </summary>
    public enum LocationType
    {
        Wall = 255,
        Empty = 0,
        Destination = 254,
        Source = 1
    }

    public struct Location
    {
        public int X, Y;

        public static Location NullType
        {
            get { return new Location(-1, -1); }
        }

        public Location(int pX, int pY)
        {
            X = pX;
            Y = pY;
        }
        public override bool Equals(object obj)
        {
            if (obj == null) return (this.Equals(Location.NullType));
            else return ((((Location)obj).X == this.X) && (((Location)obj).Y == this.Y));
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }

    int[,] objGrid;
    int xLength, yLength;
    bool hitSource, hitDest;
    int recurCount = 0;
    Location source, destination;

    /// <param name="pMap">Grid representing the floorplan in a two dimentional array.</param>
    public Wavefront(int[,] pMap, int xLength,int yLength)
    {
        objGrid = pMap;
        this.xLength = xLength;
        this.yLength = yLength;
        source = Location.NullType;
        destination = Location.NullType;
    }

    /// <summary>
    /// This method will calculate the weights for all the cells on the grid.
    /// </summary>
    public void GeneratePathWeights()
    {
        while ((!(hitDest && hitSource)) || (recurCount < objGrid.GetLength(0)))
        {
            recurCount++;
            for (int y = 0; y < objGrid.GetLength(1); y++)
            {
                for (int x = 0; x < objGrid.GetLength(0); x++)
                {
                    if (objGrid[x, y] == (int)LocationType.Wall) continue;
                    else if (objGrid[x, y] == (int)LocationType.Empty)
                    {
                        if (IsAdjacentLocOfType(x, y, LocationType.Source))
                        {
                            source = GetAdjacentLowestCell(x, y);
                            objGrid[x, y] = 2;
                            hitSource = true;
                        }
                        else
                        {
                            int adjCnt = GetAdjacentLowestCount(x, y);
                            if (adjCnt != (int)LocationType.Wall) objGrid[x, y] = adjCnt + 1;
                        }
                    }
                    else if (objGrid[x, y] == (int)LocationType.Destination)
                    {
                        destination = new Location(x, y);
                        int adjCnt = GetAdjacentLowestCount(x, y);
                        if (adjCnt != (int)LocationType.Wall) hitDest = true;
                    }
                }
            }
            if (recurCount >= objGrid.GetLength(0))
            {
                if (!hitDest | !hitSource) return;
            }
        }
    }

    /// <summary>
    /// Gets the path found between the Source and the Destination.
    /// </summary>
    /// <returns>An array of Location, representing the path found.</returns>
    /// <remarks>Null returned if no path is found.</remarks>
    public Location[] GetPath()
    {
        if (destination.Equals(Location.NullType) || source.Equals(Location.NullType))
            return null;
        Location[] path = new Location[10];
        bool exitLoop = false;
        int locPointer = 0;
        while (!exitLoop)
        {

            if (locPointer == 0)
            {
                path[locPointer] = destination;

            }
            else
            {

                path[locPointer] = GetAdjacentLowestCell(path[locPointer - 1].X, path[locPointer - 1].Y);
                if (path[locPointer].Equals(source))
                {
                    exitLoop = true;
                }
            }

            if (!exitLoop)
            {
                locPointer++;
                if (locPointer == path.Length - 1)
                {
                    //Resize the array by 10 if needed.
                    Array.Resize<Location>(ref path, path.Length + 10);
                }
            }
        }

        //Again resige the array to only contain the actual path.
        Array.Resize<Location>(ref path, locPointer + 1);
        return path;
    }

    /// <summary>
    /// Gets the adjacent cell number which has the lowest count.
    /// </summary>
    private Location GetAdjacentLowestCell(int pXCord, int pYCord)
    {
        int lowestCnt = (int)LocationType.Wall;
        Location lowestCell = Location.NullType;
        if (pXCord < xLength - 1)
        {
            if (objGrid[pXCord + 1, pYCord] != (int)LocationType.Empty && objGrid[pXCord + 1, pYCord] != (int)LocationType.Destination)
            {
                if (lowestCnt > objGrid[pXCord + 1, pYCord])
                {
                    lowestCnt = objGrid[pXCord + 1, pYCord];
                    lowestCell = new Location(pXCord + 1, pYCord);
                }
            }
        }
        if (pXCord != 0 && pXCord - 1 >= 0)
        {
            if (objGrid[pXCord - 1, pYCord] != (int)LocationType.Empty && objGrid[pXCord - 1, pYCord] != (int)LocationType.Destination)
            {
                if (lowestCnt > objGrid[pXCord - 1, pYCord])
                {
                    lowestCnt = objGrid[pXCord - 1, pYCord];
                    lowestCell = new Location(pXCord - 1, pYCord);
                }
            }
        }

        if (pYCord < yLength - 1)
        {
            if (objGrid[pXCord, pYCord + 1] != (int)LocationType.Empty && objGrid[pXCord, pYCord + 1] != (int)LocationType.Destination)
            {
                if (lowestCnt > objGrid[pXCord, pYCord + 1])
                {
                    lowestCnt = objGrid[pXCord, pYCord + 1];
                    lowestCell = new Location(pXCord, pYCord + 1);
                }
            }


        }

        if (pYCord != 0 && pYCord - 1 >= 0)
        {
            if (objGrid[pXCord, pYCord - 1] != (int)LocationType.Empty && objGrid[pXCord, pYCord - 1] != (int)LocationType.Destination)
            {
                if (lowestCnt > objGrid[pXCord, pYCord - 1])
                {
                    lowestCnt = objGrid[pXCord, pYCord - 1];
                    lowestCell = new Location(pXCord, pYCord - 1);
                }
            }
        }

        return lowestCell;
    }

    /// <returns>Lowest weight.</returns>
    private int GetAdjacentLowestCount(int pXCord, int pYCord)
    {
        Location lowestCell = GetAdjacentLowestCell(pXCord, pYCord);
        if (lowestCell.Equals(Location.NullType))
            return (int)LocationType.Wall;
        else
            return objGrid[lowestCell.X, lowestCell.Y];
    }

    /// <summary>
    /// Checks if any of the adjacent cells of of the required type.
    /// </summary>
    private bool IsAdjacentLocOfType(int pXCord, int pYCord, LocationType pType)
    {
        if (pXCord < objGrid.GetLength(0) - 1)
        {
            if (objGrid[pXCord + 1, pYCord] == (int)pType)
            {
                return true;
            }

            if (pXCord - 1 >= 0)
            {
                if (objGrid[pXCord - 1, pYCord] == (int)pType)
                {
                    return true;
                }
            }
        }

        if (pYCord < objGrid.GetLength(1) - 1)
        {
            if (objGrid[pXCord, pYCord + 1] == (int)pType)
            {
                return true;
            }

            if (pYCord - 1 >= 0)
            {
                if (objGrid[pXCord, pYCord - 1] == (int)pType)
                {
                    return true;
                }
            }
        }

        return false;
    }

    public void GeneratePathWeightsB()
    {

        while ((!(hitDest && hitSource)) || (recurCount < xLength))
        {
            recurCount++;
            for (int y = 0; y < yLength; y++)
            {
                for (int x = 0; x < xLength; x++)
                {
                    if (objGrid[x, y] == (int)LocationType.Wall) continue;
                    else if (objGrid[x, y] == (int)LocationType.Empty)
                    {
                        if (IsAdjacentLocOfType(x, y, LocationType.Source))
                        {
                            source = GetAdjacentLowestCell(x, y);
                            objGrid[x, y] = 2;
                            hitSource = true;
                        }
                        else
                        {
                            int adjCnt = GetAdjacentLowestCount(x, y);
                            if (adjCnt != (int)LocationType.Wall) objGrid[x, y] = adjCnt + 1;
                        }
                    }
                    else if (objGrid[x, y] == (int)LocationType.Destination)
                    {
                        destination = new Location(x, y);
                        int adjCnt = GetAdjacentLowestCount(x, y);
                        if (adjCnt != (int)LocationType.Wall) hitDest = true;
                    }
                }
            }
            if (recurCount >= xLength)
            {
                if (!hitDest | !hitSource) return;
            }
        }
    }
}

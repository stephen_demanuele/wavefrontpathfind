﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Mapper
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCreateTable_Click(object sender, EventArgs e)
        {
            int columns,rows ;
            Int32.TryParse(tbColumns.Text, out columns);
            Int32.TryParse(tbRows.Text, out rows);
            DataTableMap dtm = new DataTableMap(columns, rows);
            dgvMap.DataSource = dtm;
            for (int col = 0; col < dgvMap.Columns.Count; col++) dgvMap.Columns[col].Width = 50;
            for (int row = 0; row < dgvMap.Rows.Count; row++) dgvMap.Rows[row].Height = 50;
            dgvMap.Size = new Size((dtm.Columns.Count * 50) + 100, (dtm.Rows.Count * 50) + 50);
            dgvMap.Focus();
        }

        private void btnReadTable_Click(object sender, EventArgs e)
        {
            string content = DataTableMap.readTableToString((DataTableMap)dgvMap.DataSource);
            Clipboard.SetText(content, TextDataFormat.Text);
            MessageBox.Show("Clipboard has \r\n" + content);
        }

        private void btnFindPath_Click(object sender, EventArgs e)
        {
            try
            {
                DataTableMap dtSource = (DataTableMap)dgvMap.DataSource;
                if (dtSource == null)
                {
                    MessageBox.Show("invalid datasource");
                    return;
                }
                if (!dtSource.hasDest_and_Source())
                {
                    MessageBox.Show("Destination or source are missing!");
                    return;
                }
                int[,] map = createMap(dtSource.Columns.Count, dtSource.Rows.Count, ref dtSource);
                if (map == null)
                {
                    MessageBox.Show("invalid map");
                    return;
                }
                else Clipboard.SetText(MapStringify(map,dtSource.Columns.Count, dtSource.Rows.Count));
                Wavefront waveFront = new Wavefront(map, dtSource.Columns.Count, dtSource.Rows.Count);
                
                waveFront.GeneratePathWeightsB();
                Wavefront.Location[] path = waveFront.GetPath();
                if (path == null)
                {
                    MessageBox.Show("null path. sorry");
                    return;
                }
                dtSource.drawPath(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show("find path\r\n" + ex.Message);
            }
        }

        /// <summary>
        /// reads a datatable into a 2-dim array, based on axis' lengths
        /// </summary>
        /// <param name="dt">the referenced datatable, in this case the dgvDatasource</param>
        /// <returns>2-dimensional int array containing translated values</returns>
        private int[,] createMap(int xLength, int yLength, ref DataTableMap dtm)
        {
            int[,] map = new int[xLength, yLength];
            for (int y = 0; y < dtm.Rows.Count; y++)
            {
                for (int x = 0; x < dtm.Columns.Count; x++) map[x, y] = DataTableMap.translateValue(dtm.Rows[y][x].ToString());
            }
            return map;
        }

        private void btnClearPath_Click(object sender, EventArgs e)
        {
            try
            {
                DataTableMap dtm = (DataTableMap)dgvMap.DataSource;
                if (dtm != null) dtm.ClearPath();
            }
            catch (Exception ex)
            {
                MessageBox.Show("clear path\r\n" + ex.Message);
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            DataTableMap dtm = (DataTableMap)dgvMap.DataSource;
            if (dtm == null)
            {
                MessageBox.Show("No datatable to save");
                return;
            }
            else
            {
                DialogResult dr = sfd.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    dtm.Serialise(sfd.FileName);
                }
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            DialogResult dr = ofd.ShowDialog();
            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                DataTableMap dtm = DataTableMap.Deserialise(ofd.FileName);
                if (dtm != null)
                {
                    dgvMap.DataSource = dtm;
                    for (int col = 0; col < dgvMap.Columns.Count; col++) dgvMap.Columns[col].Width = 50;
                    for (int row = 0; row < dgvMap.Rows.Count; row++) dgvMap.Rows[row].Height = 50;
                    dgvMap.Size = new Size((dtm.Columns.Count * 50) + 100, (dtm.Rows.Count * 50) + 50);
                    dgvMap.Focus();
                }
                else MessageBox.Show("Could not load file");
            }
        }

        private void btnClearTable_Click(object sender, EventArgs e)
        {
            DataTableMap dtm = new DataTableMap(dgvMap.Columns.Count,dgvMap.Rows.Count);
            dgvMap.DataSource = dtm;
            for (int col = 0; col < dgvMap.Columns.Count; col++) dgvMap.Columns[col].Width = 50;
            for (int row = 0; row < dgvMap.Rows.Count; row++) dgvMap.Rows[row].Height = 50;
            dgvMap.Size = new Size((dtm.Columns.Count * 50) + 100, (dtm.Rows.Count * 50) + 50);
            dgvMap.Focus();

        }

        private string MapStringify(int[,] map, int xLength,int yLength)
        {
            StringBuilder strBuilder = new StringBuilder("int[][] map = new int[][] {");
            for (int x = 0; x < xLength; x++)
            {
                strBuilder.Append("new int[] {");
                for (int y = 0; y < yLength; y++)
                {
                    strBuilder.Append(map[x, y].ToString());
                    if (y != yLength - 1) strBuilder.Append(",");
                    else strBuilder.Append("}");
                }
                if (x != xLength - 1) strBuilder.Append(",");
                else strBuilder.Append("};");
            }
            return strBuilder.ToString();
        }
    }
}

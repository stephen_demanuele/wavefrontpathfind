﻿namespace Mapper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvMap = new System.Windows.Forms.DataGridView();
            this.tbColumns = new System.Windows.Forms.TextBox();
            this.tbRows = new System.Windows.Forms.TextBox();
            this.btnCreateTable = new System.Windows.Forms.Button();
            this.lblColumns = new System.Windows.Forms.Label();
            this.lblRows = new System.Windows.Forms.Label();
            this.pnlMap = new System.Windows.Forms.Panel();
            this.btnReadTable = new System.Windows.Forms.Button();
            this.btnFindPath = new System.Windows.Forms.Button();
            this.btnClearPath = new System.Windows.Forms.Button();
            this.gbPath = new System.Windows.Forms.GroupBox();
            this.gbSaveLoad = new System.Windows.Forms.GroupBox();
            this.btnLoad = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.sfd = new System.Windows.Forms.SaveFileDialog();
            this.gbTable = new System.Windows.Forms.GroupBox();
            this.btnClearTable = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMap)).BeginInit();
            this.pnlMap.SuspendLayout();
            this.gbPath.SuspendLayout();
            this.gbSaveLoad.SuspendLayout();
            this.gbTable.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgvMap
            // 
            this.dgvMap.AllowUserToAddRows = false;
            this.dgvMap.AllowUserToResizeColumns = false;
            this.dgvMap.AllowUserToResizeRows = false;
            this.dgvMap.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMap.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMap.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMap.ColumnHeadersVisible = false;
            this.dgvMap.Location = new System.Drawing.Point(3, 3);
            this.dgvMap.Name = "dgvMap";
            this.dgvMap.Size = new System.Drawing.Size(149, 162);
            this.dgvMap.TabIndex = 0;
            // 
            // tbColumns
            // 
            this.tbColumns.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbColumns.Location = new System.Drawing.Point(18, 33);
            this.tbColumns.Name = "tbColumns";
            this.tbColumns.Size = new System.Drawing.Size(52, 22);
            this.tbColumns.TabIndex = 1;
            // 
            // tbRows
            // 
            this.tbRows.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRows.Location = new System.Drawing.Point(76, 33);
            this.tbRows.Name = "tbRows";
            this.tbRows.Size = new System.Drawing.Size(52, 22);
            this.tbRows.TabIndex = 2;
            // 
            // btnCreateTable
            // 
            this.btnCreateTable.Location = new System.Drawing.Point(10, 23);
            this.btnCreateTable.Name = "btnCreateTable";
            this.btnCreateTable.Size = new System.Drawing.Size(75, 23);
            this.btnCreateTable.TabIndex = 3;
            this.btnCreateTable.Text = "Create";
            this.btnCreateTable.UseVisualStyleBackColor = true;
            this.btnCreateTable.Click += new System.EventHandler(this.btnCreateTable_Click);
            // 
            // lblColumns
            // 
            this.lblColumns.AutoSize = true;
            this.lblColumns.Location = new System.Drawing.Point(15, 17);
            this.lblColumns.Name = "lblColumns";
            this.lblColumns.Size = new System.Drawing.Size(47, 13);
            this.lblColumns.TabIndex = 4;
            this.lblColumns.Text = "Columns";
            // 
            // lblRows
            // 
            this.lblRows.AutoSize = true;
            this.lblRows.Location = new System.Drawing.Point(73, 17);
            this.lblRows.Name = "lblRows";
            this.lblRows.Size = new System.Drawing.Size(34, 13);
            this.lblRows.TabIndex = 5;
            this.lblRows.Text = "Rows";
            // 
            // pnlMap
            // 
            this.pnlMap.AutoScroll = true;
            this.pnlMap.AutoSize = true;
            this.pnlMap.Controls.Add(this.dgvMap);
            this.pnlMap.Location = new System.Drawing.Point(12, 190);
            this.pnlMap.Name = "pnlMap";
            this.pnlMap.Padding = new System.Windows.Forms.Padding(5);
            this.pnlMap.Size = new System.Drawing.Size(160, 173);
            this.pnlMap.TabIndex = 6;
            // 
            // btnReadTable
            // 
            this.btnReadTable.Location = new System.Drawing.Point(10, 52);
            this.btnReadTable.Name = "btnReadTable";
            this.btnReadTable.Size = new System.Drawing.Size(75, 23);
            this.btnReadTable.TabIndex = 7;
            this.btnReadTable.Text = "Read";
            this.btnReadTable.UseVisualStyleBackColor = true;
            this.btnReadTable.Click += new System.EventHandler(this.btnReadTable_Click);
            // 
            // btnFindPath
            // 
            this.btnFindPath.Location = new System.Drawing.Point(16, 23);
            this.btnFindPath.Name = "btnFindPath";
            this.btnFindPath.Size = new System.Drawing.Size(75, 23);
            this.btnFindPath.TabIndex = 8;
            this.btnFindPath.Text = "Find path";
            this.btnFindPath.UseVisualStyleBackColor = true;
            this.btnFindPath.Click += new System.EventHandler(this.btnFindPath_Click);
            // 
            // btnClearPath
            // 
            this.btnClearPath.Location = new System.Drawing.Point(16, 59);
            this.btnClearPath.Name = "btnClearPath";
            this.btnClearPath.Size = new System.Drawing.Size(75, 23);
            this.btnClearPath.TabIndex = 9;
            this.btnClearPath.Text = "Clear path";
            this.btnClearPath.UseVisualStyleBackColor = true;
            this.btnClearPath.Click += new System.EventHandler(this.btnClearPath_Click);
            // 
            // gbPath
            // 
            this.gbPath.Controls.Add(this.btnClearPath);
            this.gbPath.Controls.Add(this.btnFindPath);
            this.gbPath.Location = new System.Drawing.Point(260, 17);
            this.gbPath.Name = "gbPath";
            this.gbPath.Size = new System.Drawing.Size(107, 100);
            this.gbPath.TabIndex = 10;
            this.gbPath.TabStop = false;
            this.gbPath.Text = "Path";
            // 
            // gbSaveLoad
            // 
            this.gbSaveLoad.Controls.Add(this.btnLoad);
            this.gbSaveLoad.Controls.Add(this.btnSave);
            this.gbSaveLoad.Location = new System.Drawing.Point(385, 17);
            this.gbSaveLoad.Name = "gbSaveLoad";
            this.gbSaveLoad.Size = new System.Drawing.Size(108, 118);
            this.gbSaveLoad.TabIndex = 11;
            this.gbSaveLoad.TabStop = false;
            this.gbSaveLoad.Text = "Save / Load";
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(16, 59);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(75, 23);
            this.btnLoad.TabIndex = 1;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(16, 23);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // ofd
            // 
            this.ofd.FileName = "openFileDialog1";
            // 
            // gbTable
            // 
            this.gbTable.Controls.Add(this.btnClearTable);
            this.gbTable.Controls.Add(this.btnCreateTable);
            this.gbTable.Controls.Add(this.btnReadTable);
            this.gbTable.Location = new System.Drawing.Point(149, 17);
            this.gbTable.Name = "gbTable";
            this.gbTable.Size = new System.Drawing.Size(93, 118);
            this.gbTable.TabIndex = 12;
            this.gbTable.TabStop = false;
            this.gbTable.Text = "Table";
            // 
            // btnClearTable
            // 
            this.btnClearTable.Location = new System.Drawing.Point(10, 81);
            this.btnClearTable.Name = "btnClearTable";
            this.btnClearTable.Size = new System.Drawing.Size(75, 23);
            this.btnClearTable.TabIndex = 8;
            this.btnClearTable.Text = "Clear";
            this.btnClearTable.UseVisualStyleBackColor = true;
            this.btnClearTable.Click += new System.EventHandler(this.btnClearTable_Click);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(519, 454);
            this.Controls.Add(this.gbTable);
            this.Controls.Add(this.gbSaveLoad);
            this.Controls.Add(this.gbPath);
            this.Controls.Add(this.pnlMap);
            this.Controls.Add(this.lblRows);
            this.Controls.Add(this.lblColumns);
            this.Controls.Add(this.tbRows);
            this.Controls.Add(this.tbColumns);
            this.Name = "Form1";
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dgvMap)).EndInit();
            this.pnlMap.ResumeLayout(false);
            this.gbPath.ResumeLayout(false);
            this.gbSaveLoad.ResumeLayout(false);
            this.gbTable.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvMap;
        private System.Windows.Forms.TextBox tbColumns;
        private System.Windows.Forms.TextBox tbRows;
        private System.Windows.Forms.Button btnCreateTable;
        private System.Windows.Forms.Label lblColumns;
        private System.Windows.Forms.Label lblRows;
        private System.Windows.Forms.Panel pnlMap;
        private System.Windows.Forms.Button btnReadTable;
        private System.Windows.Forms.Button btnFindPath;
        private System.Windows.Forms.Button btnClearPath;
        private System.Windows.Forms.GroupBox gbPath;
        private System.Windows.Forms.GroupBox gbSaveLoad;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.OpenFileDialog ofd;
        private System.Windows.Forms.SaveFileDialog sfd;
        private System.Windows.Forms.GroupBox gbTable;
        private System.Windows.Forms.Button btnClearTable;
    }
}


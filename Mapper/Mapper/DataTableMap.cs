﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Mapper
{
    [Serializable]
    class DataTableMap : DataTable
    {
        public DataTableMap()
            : base()
        {
        }

        public DataTableMap(int columns, int rows)
            : base()
        {
            for (int x = 0; x < columns; x++) base.Columns.Add(new DataColumn());
            for (int y = 0; y < rows; y++) base.Rows.Add();
        }

        protected DataTableMap(SerializationInfo info, StreamingContext context) : base(info, context) { }

        public void Serialise(string filename)
        {
            Stream s = File.Open(filename, FileMode.OpenOrCreate);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(s, this);
            s.Close();
        }

        public static DataTableMap Deserialise(string filename)
        {
            if (!File.Exists(filename)) return null;
            Stream s = File.Open(filename, FileMode.Open);
            BinaryFormatter bf = new BinaryFormatter();
            DataTableMap dtm = (DataTableMap)bf.Deserialize(s);
            s.Close();
            return (DataTableMap)dtm;
        }

        public static string readTableToString(DataTableMap dtm)
        {
            if (dtm == null) return "No datatable to read";

            string content = "{\r\n";
            for (int y = 0; y < dtm.Rows.Count; y++)
            {
                content += "{";
                for (int x = 0; x < dtm.Columns.Count; x++)
                {
                    content += translateValue(dtm.Rows[y][x].ToString());
                    if (x != dtm.Columns.Count - 1) content += ",";
                }
                content += "}";
                if (y != dtm.Rows.Count - 1) content += ",\r\n";
                else content += "\r\n";
            }
            content += "}";
            return content;
        }

        public static int translateValue(string value)
        {
            if (value == string.Empty) return 0;
            else if ((value == "w") || (value == "W")) return 255;
            else if ((value == "s") || (value == "S")) return 1;
            else if ((value == "d") || (value == "D")) return 254;
            else return -1;
        }

        public void ClearPath()
        {
            for (int y = 0 ; y < base.Rows.Count ; y++)
                for (int x = 0; x < base.Columns.Count; x++)
                {
                    if (base.Rows[y][x].ToString() == "path") base.Rows[y][x] = string.Empty;
                }
        }

        public void drawPath(Wavefront.Location[] path)
        {
            foreach (Wavefront.Location lc in path) base.Rows[lc.Y][lc.X] = "path";
        }

        /// <summary>
        /// checks all cells to make sure that they have at least 1 'd' and 1 's'.
        /// Case insensitive.
        /// Has bug : you could have multiple 's' and 'd'
        /// </summary>
        /// <returns></returns>
        public bool hasDest_and_Source()
        {
            bool foundDest = false, foundSource = false;
            for (int y = 0; y < base.Rows.Count; y++)
            {
                for (int x = 0; x < base.Columns.Count; x++)
                {
                    string check = base.Rows[y][x].ToString().ToUpper();
                    if (check == "D") foundDest = true;
                    if (check == "S") foundSource = true;
                    if (foundDest && foundSource) return true;
                }
                if (foundDest && foundSource) return true;
            }
            return (foundDest && foundSource);
        }

    }
}
